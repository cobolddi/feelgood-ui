<?php @include('template-parts/header.php') ?>

<section class="BlogSinglePageSection M-Top145">
	<div class="container">
		<div class="row">
			<!-- Left Content -->
			<div class="col-md-8">
				<div class="SectionBlog BlogPostHeader">
					<h2>Praesent vel dui id enim sagittis interdum.</h2>
					<ul class="PostInfoInconBlock">
						<li>
							<a href="#">
								<span>
									<svg class="icon icon-posted-by">
										<use xlink:href="assets/img/contour.svg#icon-blog-calendar"></use>
									</svg>
								</span>
								January 24 2019
							</a>
						</li>
						<li>
							<a href="#">
								<span>
									<svg class="icon icon-posted-by">
										<use xlink:href="assets/img/contour.svg#icon-posted-by"></use>
									</svg>
								</span>
								Aarushi Sharma
							</a>
						</li>
						<li>
							<a href="#">
								<span>
									<svg class="icon icon-posted-by">
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</a>
						</li>
						<li>
							<a href="#">
								<span>
									<svg class="icon icon-posted-by">
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								25 Comments
							</a>
						</li>
						<li>
							<a href="#">
								<span>
									<svg class="icon icon-posted-by">
										<use xlink:href="assets/img/contour.svg#icon-like"></use>
									</svg>
								</span>
								25 likes
							</a>
						</li>
					</ul>
					<div class="SectionBlog BlogBannerSection">
						<img src="assets/img/temp-img/top-img.jpg" alt="banner">
					</div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id elit lobortis, ullamcorper nibh ut, tempor odio. Aliquam arcu dui, fermentum vel velit eu, elementum ultricies erat. Donec tempus facilisis sodales. Etiam sapien diam, consectetur non dui sit amet, volutpat mollis augue. Praesent sed posuere tortor, eget volutpat nisl. Mauris placerat nisl ut bibendum posuere. Nulla feugiat, dolor a fringilla consectetur, eros metus sagittis nulla, ac tempus neque lectus quis risus. In hac habitasse platea dictumst. Donec in sodales felis. Fusce commodo lacinia libero ut euismod. Vestibulum ante faucibus orci luctus et ultrices posuere cubilia Curae; Etiam id odio id velit malesuada congue.</p>
					<p>Etiam sagittis erat sed massa accumsan rhoncus. Sed ac turpis quis ipsum sagittis ultrices. Phasellus ac nulla felis. Aliquam efficitur libero eget nisl lobortis, quis ullamcorper arcu molestie. Suspendisse potenti. Duis a mollis justo. Etiam ut vehicula nunc. Praesent pulvinar sem nec purus venenatis aliquet. Donec luctus porta libero, ut interdum risus euismod id.</p>
				</div>			
				<?php 
					@include ('template-parts/CommentsBlock.php');
				?>
			</div><!--/ Left Content -->
			
			<!-- Right Content -->
			<?php 
				@include ('template-parts/RecentPostWithSearch.php');
			?>
			<!--/ Right Content -->
		</div>
	</div>
</section>



<?php @include('template-parts/footer.php') ?>