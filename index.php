<?php @include('template-parts/header.php') ?>

<?php 
	
	@include ('template-parts/PageHeader/HomeBanner.php');
	@include ('template-parts/RightContent-LeftImage.php');
	@include ('template-parts/LeftContent-RightAlignedImage.php');
	@include ('template-parts/RightContent-LeftAlignedImage.php');
	@include ('template-parts/LeftContent-RightAlignedImage.php');
	@include ('template-parts/HomeBottomSlider.php');
	@include ('template-parts/TestimonialSection.php');

?>

<?php @include('template-parts/footer.php') ?>
