<section class="Section RightContentLeftImage">
	<div class="container">
		<h2>About Sleep Systems</h2>
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="CommonImageDiv">
					<img src="assets/img/temp-img/about-us-mg.png" data-aos="fade-right" data-aos-easing="linear" >
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="RightContentBlock">
					<h4>A good night's sleep is incredibly important for your health.</h4>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

					<p class="ButtonElement" data-aos="fade-up" data-aos-delay="200" data-aos-anchor-placement="center-bottom">
						<a href="" class="Btn_SolidBackground Btn-Arrow">
							<span>
								Know More
								<svg>
									<use xlink:href="assets/img/contour.svg#icon-button-arrow-white"></use>
								</svg>
							</span>
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>