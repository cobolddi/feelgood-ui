<section class="Section HomeBottomSlider"  style="background-color: #fff">
	<div class="container">
		<h2>How adjustable bed can help us in improving the sleep</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu vehicula nunc. Pellentesque sodales sit amet orci at placerat. Pellentesque condimentum lorem non quam venenatis</p>
		<div class="slider-for" id="SVGAnimationSlider">
		    <div class="pic">
				<object class="svg" id="AnimateSvg1" type="image/svg+xml" data="assets/img/temp-img/Slider-Zero-G.svg"></object>
				<!-- <div id="AnimateSvg1" class="svg"></div> -->
		    	<!-- <svg>
					<use xlink:href="assets/img/contour.svg#icon-Slider-Zero-G"></use>
				</svg> -->
				<!-- <img src="assets/img/temp-img/Slider-Zero-G.svg" id="Slider-Zero-G"> -->
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		    <div class="pic">
				<object class="svg" id="AnimateSvg2" type="image/svg+xml" data="assets/img/temp-img/Slider-tv-position.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<!-- <img src="assets/img/temp-img/demoimg.png"> -->
				<object class="svg" id="AnimateSvg3" type="image/svg+xml" data="assets/img/temp-img/Slider-Foot_up_1.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		    <div class="pic">
				<!-- <img src="assets/img/temp-img/demoimg.png"> -->
				<object class="svg" id="AnimateSvg4" type="image/svg+xml" data="assets/img/temp-img/Slider-lounge.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<!-- <img src="assets/img/temp-img/demoimg.png"> -->
				<object class="svg" id="AnimateSvg5" type="image/svg+xml" data="assets/img/temp-img/Slider-massage.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		    <div class="pic">
				<!-- <img src="assets/img/temp-img/demoimg.png"> -->
				<object class="svg" id="AnimateSvg6" type="image/svg+xml" data="assets/img/temp-img/Slider-tv-position.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<!-- <img src="assets/img/temp-img/demoimg.png"> -->
				<object class="svg" id="AnimateSvg7" type="image/svg+xml" data="assets/img/temp-img/Slider-Foot_up_1.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		    <div class="pic">
				<!-- <img src="assets/img/temp-img/demoimg.png"> -->
				<object class="svg" id="AnimateSvg8" type="image/svg+xml" data="assets/img/temp-img/Slider-lounge.svg"></object>
				<h4>Bad Back</h4>
				<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
		    </div>
		    <div class="pic">
				<!-- <img src="assets/img/temp-img/demoimg.png"> -->
				<object class="svg" id="AnimateSvg9" type="image/svg+xml" data="assets/img/temp-img/Slider-tv-position.svg"></object>
				<h4>ZEERO G Position</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
		    </div>
		</div>

		<div class="slider-nav">
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Foot up</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Zero-G</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Head up</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Lounge</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Gym Junkie</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Massge</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Gym Junkie</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Jon Smith</p>				
			</div>
			<div class="thumb-pic">
				<img src="assets/img/temp-img/gym-junkie.png"><p>Gym Junkie</p>				
			</div>
		</div>
	</div>
</section>