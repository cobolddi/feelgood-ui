<section class="Section" style="background-color: #f5fafd;">
	<div class="LeftContentRightAllignedImage">
		<div class="SideAlignedImageDiv rightAlignedImage">
			<img src="assets/img/temp-img/motion-bed.png" data-aos="fade-left">
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="ContentBlock">
						<h2>Motion Beds</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.</p>
						<p class="ButtonElement" data-aos="fade-up" data-aos-delay="200" data-aos-anchor-placement="center-bottom">
							<a href="" class="Btn_SolidBackground Btn-Arrow">
								<span>
									Discover
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-button-arrow-white"></use>
									</svg>
								</span>
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

