<section class="HomeBanner M-Top145" style="background-color: #f5fafd;">
	<div class="SideAllignedImageDiv">
		<img src="assets/img/temp-img/banner-img.png" data-aos="fade-left">
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="HomeBannerContentBlock">
					<h1>We help people Live healthier Live happier</h1>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, magnam.</p>
					<p class="ButtonElement" data-aos="fade-up" data-aos-delay="200" data-aos-anchor-placement="center-bottom">
						<a href="" class="Btn_SolidBackground Btn-Arrow">
							<span>
								Discover
								<svg>
									<use xlink:href="assets/img/contour.svg#icon-button-arrow-white"></use>
								</svg>
							</span>
						</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
