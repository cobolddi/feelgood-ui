<section class="LeftContentRightFormSection">
	<div class="container">
		<div class="row">
			<div class="col-md-6 leftCol">
				<div class="Section Content LeftContent">
					<h2>Download Product Brochure</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur porttitor in nisl id ornare. Ut sapien lorem, auctor et egestas vel, ornare at libero. Sed ac sapien in erat</p>
					<button class="DownloadButton M-Top30">
						<span class="DownloadSpan">Download</span>
						<span class="IconSpan">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-download"></use>
							</svg>
						</span>
					</button>
				</div>
			</div>
			<div class="col-md-6">
				<div class="Section Content RightContent">
					<h2>Enquiry Form</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur porttitor in nisl id ornare. Ut sapien lorem, auctor et egestas vel, ornare at libero. Sed ac sapien in erat</p>
					
					<form>
						<div class="row">
							<div class="col-12 col-md-6">
								<input type="text" placeholder="First Name">
							</div>
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Last Name">
							</div>
							<div class="col-12 col-md-6">
								<input type="email" placeholder="Email Address">
							</div>
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Phone No.">
							</div>
							<div class="col-12 col-md-12">
								<textarea placeholder="Message"></textarea>
							</div>
							<div class="col-12 col-md-12">
								<input type="submit" value="Send Email" class="Button">
							</div>
						</div>
					</form>
					
				</div>
			</div>
		</div>
	</div>
</section>