<section class="ProductFeaturesSlider" style="background-color: #f5fafd;">
	<div class="container">
		<div class="ProductFeature">
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-Wireless-Remote"></use>
                </svg>
				<p>Wireless Remote</p>				
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-Head--Foot-Controls"></use>
                </svg>
				<p>Head & Foot Controls</p>				
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-One-touch-Flat-Position"></use>
                </svg>
				<p>One Touch Flat Position</p>				
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-Zero-Clearance"></use>
                </svg>
				<p>Zero Clearance</p>				
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-Zero-G"></use>
                </svg>
				<p>Zero G</p>				
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-Programmable-Position"></use>
                </svg>
				<p>2 Programmable Position</p>				
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-Wireless-Remote"></use>
                </svg>
				<p>Wireless Remote</p>				
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-Head--Foot-Controls"></use>
                </svg>
				<p>Head & Foot Controls</p>					
			</div>
			<div class="thumb-pic">
				<svg id="my-svg" class="animate">
                    <use xlink:href="assets/img/contour.svg#icon-One-touch-Flat-Position"></use>
                </svg>
				<p>One Touch Flat Position</p>					
			</div>
		</div>
	</div>
</section>