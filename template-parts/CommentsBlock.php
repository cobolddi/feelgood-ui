<section class="SectionBlog CommentsBlockSection">
	<div class="CommmentsBlock">
		<h4>Comments</h4>
		<ul class="CommmentsBlock__ul">
			<li class="CommmentsBlock__list">
				<div class="CommmentsBlock__firstLeval">
					<div class="CommmentsBlock__image">
						<img src="assets/img/temp-img/comment.jpg" alt="user">
					</div>
					<div class="CommmentsBlock__content">
						<h5>Mark Parker</h5>
						<small>January 24 2019</small>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id elit lobortis, ullamcorper nibh ut, tempor odio. Aliquam arcu.</p>
					</div>
				</div>
				<ul class="CommmentsBlock__secondLeval">
					<li class="CommmentsBlock__list">
						<div class="CommmentsBlock__image">
							<img src="assets/img/temp-img/comment.jpg" alt="user">
						</div>
						<div class="CommmentsBlock__content">
							<h5>Mark Parker</h5>
							<small>January 24 2019</small>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id elit lobortis, tempor odio. Aliquam arcu dui.</p>
						</div>
					</li>
				</ul>
			</li>
			<li class="CommmentsBlock__list">
				<div class="CommmentsBlock__firstLeval">
					<div class="CommmentsBlock__image">
						<img src="assets/img/temp-img/comment.jpg" alt="user">
					</div>
					<div class="CommmentsBlock__content">
						<h5>Mark Parker</h5>
						<small>January 24 2019</small>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce id elit lobortis, ullamcorper nibh ut, tempor odio arcu dui.</p>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div class="SectionBlog CommentForm">
		<h4>Leave a Comment</h4>
		<form>
			<div class="row">
				<div class="col-12 col-md-6">
					<input type="text" placeholder="Name">
				</div>
				<div class="col-12 col-md-6">
					<input type="email" placeholder="Email Address">
				</div>
				<div class="col-12 col-md-12">
					<textarea placeholder="Message"></textarea>
				</div>
				<div class="col-12 col-md-12">
					<input type="submit" value="Send" class="Button">
				</div>
			</div>
		</form>
	</div>

</section>