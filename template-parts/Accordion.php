<section class="Section AccordionSection">
	<div class="container">
		<div class="wrapper">
		    <ul class="accordion" id="accordion">
		    	<!-- 1 accordion-->
		        <li class="accordion__item is-open">
		            <div class="accordion__link js-accordion-link">Integer varius odio ut justo bibendum, vel suscipit ante venenatis?</div>
		            <div class="accordion__submenu js-accordion-submenu" >
		                <div class="accordion__submenu-item">
			                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in diam quis mi gravida sagittis. Aenean ac ullamcorper leo. Phasellus id metus vitae velit tincidunt porta vitae a sapien. Praesent leo mauris, ullamcorper quis augue nec, ultrices tincidunt elit. Integer et interdum ipsum. Nam euismod molestie metus.</p>
			            </div>
		            </div>
		        </li>
		        <!-- 2 accordion-->
		        <li class="accordion__item">
		            <div class="accordion__link js-accordion-link">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</div>
		            <div class="accordion__submenu js-accordion-submenu">
		                <div class="accordion__submenu-item">
			                <p>Proin egestas sagittis nisl et faucibus. Ut laoreet a odio ornare semper. Ut malesuada, odio sed rutrum dapibus, lorem lorem ornare quam, id aliquam ante urna ac nisl. Nam purus quam, vehicula non nibh eget, dictum elementum metus. Vestibulum pellentesque elit risus, et elementum augue tempus a. In pulvinar sapien vel metus bibendum, vel venenatis nunc iaculis. Nam ultrices lacus interdum, rutrum mauris sagittis, pulvinar est.</p>
			            </div>
		            </div>
		        </li>
		        <!-- 3 accordion-->
		        <li class="accordion__item">
		            <div class="accordion__link js-accordion-link">Praesent leo mauris, ullamcorper quis augue nec?</div>
		            <div class="accordion__submenu js-accordion-submenu">
		                <div class="accordion__submenu-item">
			                <p>Sed vitae posuere ex. Cras odio velit, rhoncus nec commodo non, tempor vitae turpis. Nam id iaculis augue. Aliquam porttitor cursus augue nec commodo. Aliquam ultricies, ante sit amet aliquet laoreet, ipsum est vestibulum sem, vitae congue libero ligula vel nibh. In velit mi, semper ac arcu nec, congue cursus erat.</p>
			            </div>
		            </div>
		        </li>
		        <!-- 4 accordion-->
		        <li class="accordion__item">
		            <div class="accordion__link js-accordion-link">Class aptent taciti sociosqu?</div>
		            <div class="accordion__submenu js-accordion-submenu">
		                <div class="accordion__submenu-item">
			                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in diam quis mi gravida sagittis. Aenean ac ullamcorper leo. Phasellus id metus vitae velit tincidunt porta vitae a sapien. Praesent leo mauris, ullamcorper quis augue nec, ultrices tincidunt elit. Integer et interdum ipsum. Nam euismod molestie metus.</p>
			            </div>
		            </div>
		        </li>
		        <!-- 5 accordion-->
		        <li class="accordion__item">
		            <div class="accordion__link js-accordion-link">Aenean mattis semper tortor, vel tincidunt orci eleifend?</div>
		            <div class="accordion__submenu js-accordion-submenu">
		                <div class="accordion__submenu-item">
			                <p>Quisque dictum felis sit amet sapien condimentum consequat id id augue. Proin lacinia quam nisl, eget malesuada diam mattis et. Duis non accumsan orci. Integer semper neque a neque pharetra facilisis. Aenean congue blandit nisi quis suscipit.</p>
			            </div>
		            </div>
		        </li>
		        <!-- 6 accordion-->
		        <li class="accordion__item">
		            <div class="accordion__link js-accordion-link">Maecenas iaculis quis ipsum ut molestie?</div>
		            <div class="accordion__submenu js-accordion-submenu">
		                <div class="accordion__submenu-item">
			                <p>Ut ultricies est eu sapien auctor, in tempus mi iaculis. Sed congue placerat nisl, eget venenatis sapien hendrerit in. Quisque dictum felis sit amet sapien condimentum consequat id id augue. Proin lacinia quam nisl, eget malesuada diam mattis et. Duis non accumsan orci. Integer semper.</p>
			            </div>
		            </div>
		        </li>
		    </ul>
		</div>
	</div>
</section>