<section class="Section ProductFeatures" style="background: #f5fafd">
	<div class="container">
		<h2>Features</h2>
		<div class="row">
			<div class="col-12 col-md-2 MobileOnly mb-4 pb-4">
				<img src="assets/img/temp-img/remote.png">
			</div>
			<div class="col-12 col-md-5 PaddingRight">
				<div class="row">
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error iure possimus quis voluptate enim praesentium.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-plus"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error iure possimus quis voluptate enim praesentium.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-minus"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error iure possimus quis voluptate enim praesentium.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error iure possimus quis voluptate enim praesentium.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error iure possimus quis voluptate enim praesentium.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-plus"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error iure possimus quis voluptate enim praesentium.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-minus"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error iure possimus quis voluptate enim praesentium.</p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-2 DesktopOnly">
				<img src="assets/img/temp-img/remote.png">
			</div>
			<div class="col-12 col-md-5 PaddingLeft">
				<div class="row">
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-Programmable-Position"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
					<div class="col-12 col-md-2">
						<svg id="my-svg" class="animate">
                            <use xlink:href="assets/img/contour.svg#icon-check"></use>
                        </svg>
					</div>
					<div class="col-12 col-md-10">
						<p>Lorem ipsum dolor sit amet, consectetur.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>