<div class="col-md-4">
	<div class="Section BlogPostRightContent">
		<?php
		@include ('template-parts/SearchBox.php');
		 ?>
		<!-- <div class="SearchBox">
			<input type="search" name="serach" placeholder="Search Post">
			<span>
				<input type="submit" name="submit" value="">
			</span>
		</div> -->
		<div class="RecentPostBox">
			<h4>Recent Post</h4>
			<ul class="RecentPostBox__ul">
				<li class="RecentPostBox__list">
					<a href="#">
						<div class="RecentPostImg">
							<img src="assets/img/temp-img/recent-post-2.jpg" alt="Image">
						</div>
						<div class="RecentPostContent">
							<h5>Prraesent vel dui id enim sagittis interdum.</h5>
							<p>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-calendare"></use>
									</svg>
								</span>
								january 24 2019
							</p>
						</div>
					</a>
				</li>
				<li class="RecentPostBox__list">
					<a href="#">
						<div class="RecentPostImg">
							<img src="assets/img/temp-img/recent-post.jpg" alt="Image">
						</div>
						<div class="RecentPostContent">
							<h5>Prraesent vel dui id enim sagittis interdum.</h5>
							<p>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-calendare"></use>
									</svg>
								</span>
								january 24 2019
							</p>
						</div>
					</a>
				</li>
				<li class="RecentPostBox__list">
					<a href="#">
						<div class="RecentPostImg">
							<img src="assets/img/temp-img/recent-post-2.jpg" alt="Image">
						</div>
						<div class="RecentPostContent">
							<h5>Prraesent vel dui id enim sagittis interdum.</h5>
							<p>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-calendare"></use>
									</svg>
								</span>
								january 24 2019
							</p>
						</div>
					</a>
				</li>
				<li class="RecentPostBox__list">
					<a href="#">
						<div class="RecentPostImg">
							<img src="assets/img/temp-img/recent-post.jpg" alt="Image">
						</div>
						<div class="RecentPostContent">
							<h5>Prraesent vel dui id enim sagittis interdum.</h5>
							<p>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-calendare"></use>
									</svg>
								</span>
								january 24 2019
							</p>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<section class="SectionBlog">
			<div class="RecentPostSocialMedia">
				<h4>Follow Us:</h4>
				<ul>
					<li class="RecentPostSocialMedia__list facebook">
						<a href="#">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-face"></use>
							</svg>
						</a>
					</li>
					<li class="RecentPostSocialMedia__list insta">
						<a href="#">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-insta"></use>
							</svg>
						</a>
					</li>
					<li class="RecentPostSocialMedia__list linkdin">
						<a href="#">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-in"></use>
							</svg>
						</a>
					</li>
					<li class="RecentPostSocialMedia__list twitter">
						<a href="#">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-twit"></use>
							</svg>
						</a>
					</li>
				</ul>
			</div>
		</section>
		

	</div>
</div>