<section class="Section ThreeBoxImage">
	<div class="container">
		<div class="ThreeBoxImageHead">
			<h2>Adjustable Beds</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu vehicula nunc. Pellentesque sodales sit amet orci at placerat. Pellentesque condimentum lorem non quam venenatis</p>
		</div>
		<div class="row">
			<!-- Box 1 -->
			<div class="col-12 col-md-4 M-Bottom20">
				<div class="BoxWrap">
					<a href="#">
						<div class="BoxImage">
							<div class="TransparentBackground">
								<div class="hoverIcon">
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-vew-icon"></use>
									</svg>
									<p>View Details</p>
								</div>
							</div>
							<img src="assets/img/temp-img/conroue-elite.jpg" alt="image">
						</div>
						<div class="BoxContent">
							<h5>Essence</h5>
						</div>
					</a>
				</div>
			</div>
			<!-- Box 2 -->
			<div class="col-12 col-md-4 M-Bottom20">
				<div class="BoxWrap">
					<a href="#">
						<div class="BoxImage">
							<div class="TransparentBackground">
								<div class="hoverIcon">
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-vew-icon"></use>
									</svg>
									<p>View Details</p>
								</div>
							</div>
							<img src="assets/img/temp-img/conroue-elite.jpg" alt="image">
						</div>
						<div class="BoxContent">
							<h5>Pure Slim</h5>
						</div>
					</a>
				</div>
			</div>
			<!-- Box 3 -->
			<div class="col-12 col-md-4 M-Bottom20">
				<div class="BoxWrap">
					<a href="#">
						<div class="BoxImage">
							<div class="TransparentBackground">
								<div class="hoverIcon">
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-vew-icon"></use>
									</svg>
									<p>View Details</p>
								</div>
							</div>
							<img src="assets/img/temp-img/conroue-elite.jpg" alt="image">
						</div>
						<div class="BoxContent">
							<h5>Contour Elite</h5>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>