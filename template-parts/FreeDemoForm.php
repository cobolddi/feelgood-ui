<section class="Section FreeDemoSection" style="background-color: #f5fafd;">
	<div class="container">
		<div class="SmallContainer">
			<h2>Free Demo</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo voluptatem quo, sed laborum, incidunt in ullam veritatis cumque quia repellat quam esse saepe doloremque perferendis blanditiis ratione architecto. Harum vel quibusdam, sunt nostrum beatae, odit quisquam quasi numquam fuga saepe voluptatem repellat ab.</p>
			<div class="FormDiv">
				<form>
					<div class="row">
						<div class="col-12 col-md-6">
							<input type="text" placeholder="Name">
						</div>
						<div class="col-12 col-md-6">
							<input type="email" placeholder="Email Address">
						</div>
						<div class="col-12 col-md-6">
							<input type="text" placeholder="Phone No.">
						</div>
						<div class="col-12 col-md-6">
							<input type="text" placeholder="Location">	
						</div>
						<div class="col-12 col-md-6">
							<input type="text" name="datefilter" value="" placeholder="Date" />
							<!-- <input type="text" class="Date" onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Date">						 -->
						</div>
						<div class="col-12 col-md-6">
							<input type="text" onfocus="(this.type='time')" onblur="(this.type='text')" placeholder="Time">
						</div>
						<div class="col-12 col-md-6">
							<select required>
								<option value="" disabled selected>State</option>
								<option value="saab">Saab</option>
								<option value="opel">Opel</option>
								<option value="audi">Audi</option>
							</select>
						</div>
						<div class="col-12 col-md-6">
							<input type="text" placeholder="Additional information">
						</div>
						<div class="col-12 col-md-12">
							<textarea placeholder="Message"></textarea>
						</div>
						<div class="col-12 col-md-12">
							<input type="submit" value="Send" class="Button">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>