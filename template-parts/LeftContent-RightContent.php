<section class="Section LeftContentRightContentSection" style="background: #fff">
	<div class="container">
		<div class="row">
			<div class="col-md-6 ColLeft">
				<div class="ContentBox LeftContentBox">
					<h2>Warranty & Service</h2>
					<h4>Hours of Operation:</h4>
					<h4>Monday - Friday 9AM - 7PM</h4>
					<h4>
						<span>
							<!-- <img src="assets/img/phone.svg" alt="Phone"> -->
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-phone-blue"></use>
							</svg>
						</span>
						<a href="#">+91 11 4659-4898</a>
					</h4>
					<h4>
						<span>
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-email-blue"></use>
							</svg>
						</span>
						<a href="#">customerservice@feelgood.com</a>
					</h4>
				</div>
			</div>
			<div class="col-md-6">
				<div class="ContentBox RightContentBox">
					<h2>Order Information</h2>
					<h4>Hours of Operation:</h4>
					<h4>Monday - Friday 9AM - 7PM</h4>
					<h4>
						<span>
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-phone-blue"></use>
							</svg>
						</span>
						<a href="#">+91 11 4659-4898</a>
					</h4>
					<h4>
						<span>
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-email-blue"></use>
							</svg>
						</span>
						<a href="#">customerservice@feelgood.com</a>
					</h4>
				</div>
			</div>
		</div>
	</div>
</section>