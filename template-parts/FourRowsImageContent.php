<section class="Section FourRowsImageContent">
<!-- first row left image right content -->
	<div class="Section FirstRow-LeftImageRightContent">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="ImageSection CommonImageDiv">
						<img src="assets/img/temp-img/sleep-2.jpg" alt="image" data-aos="fade-down">
					</div>
				</div>
				<div class="col-md-6">
					<div class="ContentSection">
						<h2>How we can improve the sleep</h2>
						<p>Usce vitae pulvinar odio. Ut lacinia fringilla erat vitae tempor. Fusce elementum accumsan nibh. Quisque vestibulum ante quis metus semper venenatis. Pellentesque odio arcu, aliquam non justo.
						Sed ac nunc orci. Aenean luctus tempus lectus, et pulvinar.</p>
						<p>Duis urna tortor, aliquet ut leo sit amet, consequat volutpat lacus. Duis a eros feugiat, vulputate arcu eget, vestibulum metus. Integer tincidunt sem eu tortor posuere dictum. Integer interdum ipsum a risus ullamcorper suscipit. Donec interdum, diam vitae sodales faucibus, enim odio elementum odio.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- second row Full image -->
	<div class="SecondRow-FullImage CommonImageDiv">
		<img src="assets/img/temp-img/sleep-3.jpg" alt="image">
	</div>
<!-- third row left content right image -->
	<div class="Section ThirdRow-LeftContentRightImage">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="ContentSection">
						<h2>How adjustable bed can help us in improving the sleep</h2>
						<h4>Sleep ZERo G</h4>
						<p>Quisque porta non sem sit amet pharetra. Sed metus ligula, gravida tincidunt auctor vitae, accumsan ut nibh. Suspendisse suscipit condimentum elit non auctor. Etiam pellentesque libero turpis, facilisis dignissim nisi blandit sed. Vestibulum quis mi a mauris gravida ultrices. Proin aliquet feugiat vestibulum.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ImageSection CommonImageDiv">
						<img src="assets/img/temp-img/sleep-4.jpg" alt="image" data-aos="fade-up">
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- four row left image right content -->
	<div class="Section FourRow-LeftImageRightContent">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="ImageSection CommonImageDiv">
						<img src="assets/img/temp-img/sleep-5.jpg" alt="image" data-aos="fade-right">
					</div>
				</div>
				<div class="col-md-6">
					<div class="ContentSection">
						<p>Usce vitae pulvinar odio. Ut lacinia fringilla erat vitae tempor. Fusce elementum accumsan nibh. Quisque vestibulum ante quis metus semper venenatis. Pellentesque odio arcu, aliquam non justo.
						Sed ac nunc orci. Aenean luctus tempus lectus, et pulvinar.</p>
						<p>Duis urna tortor, aliquet ut leo sit amet, consequat volutpat lacus. Duis a eros feugiat, vulputate arcu eget, vestibulum metus. Integer tincidunt sem eu tortor posuere dictum. Integer interdum ipsum a risus ullamcorper suscipit. Donec interdum, diam vitae sodales faucibus, enim odio elementum odio.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>