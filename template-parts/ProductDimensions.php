<section class="Section ProductDimension">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="TitleBox">
					<h2>Dimensions</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="ProductDimensionBox FirstBox">
					<img class="Logo" src="assets/img/temp-img/TXL.png">
					<h2>TXL</h2>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="ProductDimensionBox SecondBox">
					<img class="Logo" src="assets/img/temp-img/TXL.png">
					<h2>King</h2>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="ProductDimensionBox ThirdBox">
					<img class="Logo" src="assets/img/temp-img/TXL.png">
					<h2>Queen</h2>
				</div>
			</div>
		</div>
	</div>
</section>