<section class="Section ProductComparison">
	<div class="container">
		<div class="TableBox">
			<table>
				<tr>
					<td><h2>Comparison Sheet of the Motion Beds</h2></td>
					<td><img src="assets/img/temp-img/essence.jpg"></td>
					<td><img src="assets/img/temp-img/essence.jpg"></td>
					<td><img src="assets/img/temp-img/essence.jpg"></td>
				</tr>
				<tr>
					<td><b>Features</b></td>
					<td><b>Essence</b></td>
					<td><b>Pure Slim</b></td>
					<td><b>Contour Elite</b></td>
				</tr>
				<tr>
					<td>Remote</td>
					<td>6 Button Remote</td>
					<td>8 Button</td>
					<td>15 Button Remote</td>
				</tr>
				<tr>
					<td>Wireless Remote</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Head & Foot Control</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>One Touch Flat Positions</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>2 Programmable Positions</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Zero Clearance</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Zero G</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Zero Standby Power System</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Power Outage Protection</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Silent Drive Motors</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Wall Saver</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Massage</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Auto Head Tilt</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Dual USB Ports</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Bluetooth</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Underbed Lighting</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Extension Deck</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
				<tr>
					<td>Leg Options</td>
					<td>
						Black Metal 6+3 legs
					</td>
					<td>
						Black Metal 6+3 legs
					</td>
					<td>
						Wood Tapered 5+3
					</td>
				</tr>			
				<tr>
					<td colspan="4" class="FullWidth">
						Accessory Options
					</td>
				</tr>
				<tr>
					<td>Extension Deck</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-close"></use>
	                    </svg>
					</td>
					<td>
						<svg id="my-svg" class="animate">
	                        <use xlink:href="assets/img/contour.svg#icon-check"></use>
	                    </svg>
					</td>
				</tr>
			</table>
		</div>
		<p>
			<a href="" class="Btn_SolidBackground Btn-Arrow">
				<span>
					Know More
					<svg>
						<use xlink:href="assets/img/contour.svg#icon-button-arrow-white"></use>
					</svg>
				</span>
			</a>
		</p>
	</div>
</section>