<section class="Section LeftRightIconContentSection">
	<div class="container">
		<div class="row">
			<div class="col-md-6 M-Bottom20">
				<div class="row">
					<div class="col-3">
						<div class="IconWrap">
							<!-- <img src="assets/img/massage.png" alt="Icon"> -->
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-Better-Blood-Circulation"></use>
							</svg>
						</div>
					</div>
					<div class="col-9">
						<h4>Better Blood Circulation</h4>
						<p>Sed placerat convallis tristique. Duis vel luctus ac lacinia leo. Ut ultricies est eu sapien auctor, in tempus mi iaculis. Sed congue placerat nisl, eget venenatis sapien hendrerit in. Quisque dictum felis sit sapien condimentum consequat id augue.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 M-Bottom20">
				<div class="row">
					<div class="col-3">
						<div class="IconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-Relieve-Asthma"></use>
							</svg>
						</div>
					</div>
					<div class="col-9">
						<h4>Relieve Asthama And Sleep Apnea</h4>
						<p>Duis sodales mauris erat, ut accumsan lacus blandit eu. Pellentesque in leo eget mi ultricies mollis eget a arcu. Praesent non neque lectus. Donec tincidunt blandit erat.</p>
					</div>
				</div>
			</div>
			<!-- 2nd Line -->
			<div class="col-md-6 M-Bottom20">
				<div class="row">
					<div class="col-3">
						<div class="IconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-Improved-Digestion"></use>
							</svg>
						</div>
					</div>
					<div class="col-9">
						<h4>Improve Digestion</h4>
						<p>Proin egestas sagittis nisl et faucibus. Ut laoreet a odio ornare semper. Ut malesuada, odio sed rutrum dapibus, lorem lorem ornare quam, id aliquam ante urna ac nisl. Nam purus quam, vehicula non nibh eget.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 M-Bottom20">
				<div class="row">
					<div class="col-3">
						<div class="IconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-Better-Sleep"></use>
							</svg>
						</div>
					</div>
					<div class="col-9">
						<h4>Better Sleep For Insomniacs</h4>
						<p>Duis sodales mauris erat, ut accumsan lacus blandit eu. Pellentesque in leo eget mi ultricies mollis eget a arcu. Praesent non neque lectus. Donec tincidunt blandit erat.</p>
					</div>
				</div>
			</div>
			<!-- 3rd Line -->
			<div class="col-md-6 M-Bottom20">
				<div class="row">
					<div class="col-3">
						<div class="IconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-Reduced-Leg-Swelling"></use>
							</svg>
						</div>
					</div>
					<div class="col-9">
						<h4>Reduced Leg Swelling</h4>
						<p>Proin egestas sagittis nisl et faucibus. Ut laoreet a odio ornare semper. Ut malesuada, odio sed rutrum dapibus, lorem lorem ornare quam, id aliquam ante urna ac nisl. Nam purus quam, vehicula non nibh eget.</p>
					</div>
				</div>
			</div>
			<div class="col-md-6 M-Bottom20">
				<div class="row">
					<div class="col-3">
						<div class="IconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-Ease-Of-Getting"></use>
							</svg>
						</div>
					</div>
					<div class="col-9">
						<h4>Ease of Getting Out Of Bed</h4>
						<p>Duis sodales mauris erat, ut accumsan lacus blandit eu. Pellentesque in leo eget mi ultricies mollis eget a arcu. Praesent non neque lectus. Donec tincidunt blandit erat.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>