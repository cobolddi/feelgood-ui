<section class="Section LeftContentRightImageSection">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="ContentBlock">
					<h2>Importance of sleep</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisque porta non sem sit amet pharetra. Sed metus ligula, gravida tincidunt auctor vitae, accumsan ut nibh. Suspendisse suscipit condimentum elit non auctor.</p>
					<p>Maecenas et fermentum dui, ac dapibus magna. Nullam id massa ac massa eleifend gravida. Phasellus dignissim tempus suscipit. Aliquam vitae ligula faucibus metus molestie tincidunt.</p>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="CommonImageDiv">
					<img src="assets/img/temp-img/sleep-1.jpg" data-aos="fade-left">
				</div>
			</div>
		</div>
	</div>
</section>