<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Feel Good</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<link href="assets/css/vendor.min.css" rel="stylesheet">
	<link href="assets/css/styles.min.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/png" href="assets/img/feel-good-fab-icon.png"/>
</head>

<body>
	<header id="navbar">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-12">
					<a href="index.php"><img class="Logo" src="assets/img/logo.png"></a>
				</div>				
			</div>
		</div>
		<div class="HeaderMenuBox">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-12">
						<div class="MenuHeader">
							<nav>
								<div class="DesktopMenu">
									<ul>
										<li>
											<a href="index.php" class="current-menu-item">Home</a>
										</li>
										<li class="menu-item menu-item-has-children">
											<a href="sleep-system.php">Sleep Systems</a>
											<ul class="sub-menu">
												<li><a href="Product.php">Essence</a></li>
												<li><a href="Product.php">Pure Slim</a></li>
												<li><a href="Product.php">Contour Elite</a></li>
												<li><a href="">Mattress</a></li>
												<li><a href="">Pillows</a></li>
											</ul>
										</li>
										<li>
											<a href="importance-of-sleep.php">Importance of Sleep</a>
										</li>
										<li class="menu-item menu-item-has-children">
											<a href="benefits-of-sleep-system.php">Benefits of Sleep System</a>
											<ul class="sub-menu">
												<li><a href="">USP</a></li>
												<li><a href="">Testimonials</a></li>
												<li><a href="">Sitemap</a></li>
											</ul>
										</li>
										<li>
											<a href="FreeDemo.php">Free Demo</a>
										</li>
										<li>
											<a href="">Support</a>
										</li>
										<li>
											<a href="Blog.php">Blogs</a>
										</li>
										<li>
											<a href="ContactUs.php">Contact Us</a>
										</li>
									</ul>
								</div>

								<div class="MobileMenu">
								    <button class="grt-mobile-button">
								        <div class="line1"></div>
								        <div class="line2"></div>
								        <div class="line3"></div>
								    </button>
							    
								    <ul>
								        <li class="active"><a href="">Home</a></li>
								        <li>
											<a href="">Sleep Systems</a>
											<ul>
												<li><a href="">Adjustable Beds</a></li>
												<li><a href="">Mattress</a></li>
												<li><a href="">Pillows</a></li>
											</ul>
										</li>
										<li>
											<a href="importance-of-sleep.php">Importance of Sleep</a>
										</li>
								        <li>
											<a href="">Benefits of Sleep System</a>
											<ul>
												<li><a href="">USP</a></li>
												<li><a href="">Testimonials</a></li>
												<li><a href="">Sitemap</a></li>
											</ul>
										</li>
										<li><a href="FreeDemo.php">Free Demo</a></li>
										<li><a href="">Support</a></li>
										<li><a href="Blog.php">Blogs</a></li>
								        
								        <li><a href="ContactUs.php">Contact Us</a></li>
								    </ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<main>