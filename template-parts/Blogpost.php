<section class="Section BlogsPageSection">
	<div class="container">
		<h4>Categories</h4>
		<div class="row FilterButtonRow">
			<div class="col-md-8">
				<div class="CategoriesFilterButton">
				  <button class="is_active" data-filter="*">All Posts</button>
				  <button data-filter=".PostType1">Motion Beds</button>
				  <button data-filter=".PostType2">Mattress</button>
				  <button data-filter=".PostType3">Pillows</button>
				</div>
			</div>
			<div class="col-md-4">
				<?php
					@include ('template-parts/SearchBox.php');
				 ?>
			</div>	
		</div>
		<div id="AllPostCategories" class="row">
			<!-- Post type 1 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType1">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Vestibulum retrum massa eu metus hendrerit aliquam.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 2 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType2">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Praesent vel dui id enim sagittis interdum.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 3 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType3">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Pellentesque auctor, nunc vitae congue malesuada.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 2 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType2">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Etiam sagittis erat sed massa accumsan rhoncus.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 1 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType1">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Morbi eu vestibulum est ccras tempus purus a rutrum varius.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 2 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType2">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Praesent vel dui id enim sagittis interdum.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 3 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType3">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Praesent vel dui id enim sagittis interdum.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 1 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType1">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Praesent vel dui id enim sagittis interdum.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
			<!-- Post type 2 -->
			<div class="col-md-6 col-lg-4 PostCategories PostType2">
				<div class="PostCategories__box">
					<a href="BlogSinglePage.php">
						<div class="PostCategories__image">
							<img src="assets/img/temp-img/350x220.png" alt="image">
						</div>
						<ul class="PostCategories__headerUl">
							<li class="active">january 24 2019</li>
							<li>By Aarushi Sharma</li>
						</ul>
						<div class="PostCategories__content">
							<h3>Praesent vel dui id enim sagittis interdum.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</div>
						<ul class="PostCategories__footerUl">
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-comments"></use>
									</svg>
								</span>
								No Comments
							</li>
							<li>
								<span>
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-view"></use>
									</svg>
								</span>
								View
							</li>
						</ul>
					</a>
				</div>
			</div>
		</div><!--/ row -->
		<div class="row">
			<div class="col-12">
				<div class="PaginationBox">
					<a href="#" class="prevPage">Previous</a>
					<ul>
						<li><a href="#" class="is_active">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
					</ul>
					<a href="#" class="nextPage">Next</a>
				</div>
			</div>
		</div>
	</div>
</section>