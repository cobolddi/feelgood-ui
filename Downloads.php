<?php @include('template-parts/header.php') ?>

<?php 
	
	@include ('template-parts/PageHeader/FullBanner.php');

?>

<section class="Section DownloadSection">
	<div class="container">
		<div class="CenterHeading Center">
			<h2>Download</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in diam quis mi gravida sagittis. Aenean ac ullamcorper leo. Phasellus id metus vitae velit.</p>
		</div>
		<div class="row">
			<div class="col-12">
				<ul class="DownloadUlWrap">
					<!-- List 1 -->
					<li>
						<div class="DownloadIconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-download-brochure"></use>
							</svg>
						</div>
						<div class="DownloadContent">
							<h3>Download Brochure</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in diam quis gravida sagittis. Aenean ac ullamcorper leo.</p>
						</div>
						<div class="DownloadButtonWrap">
							<button class="DownloadButton">
								<span class="DownloadSpan">Download</span>
								<span class="IconSpan">
									<!-- <img src="assets/img/download.svg" alt="Icon"> -->
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-download"></use>
									</svg>
								</span>
							</button>
						</div>
					</li>
					<!-- List 2 -->
					<li>
						<div class="DownloadIconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-pdf"></use>
							</svg>
						</div>
						<div class="DownloadContent">
							<h3>Download Brochure</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in diam quis gravida sagittis. Aenean ac ullamcorper leo.</p>
						</div>
						<div class="DownloadButtonWrap">
							<button class="DownloadButton">
								<span class="DownloadSpan">Download</span>
								<span class="IconSpan">
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-download"></use>
									</svg>
								</span>
							</button>
						</div>
					</li>
					<!-- List 3 -->
					<li>
						<div class="DownloadIconWrap">
							<svg>
								<use xlink:href="assets/img/contour.svg#icon-warranty"></use>
							</svg>
						</div>
						<div class="DownloadContent">
							<h3>Download Brochure</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam in diam quis gravida sagittis. Aenean ac ullamcorper leo.</p>
						</div>
						<div class="DownloadButtonWrap">
							<button class="DownloadButton">
								<span class="DownloadSpan">Download</span>
								<span class="IconSpan">
									<svg>
										<use xlink:href="assets/img/contour.svg#icon-download"></use>
									</svg>
								</span>
							</button>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>