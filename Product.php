<?php @include('template-parts/header.php') ?>

<?php 

	@include ('template-parts/PageHeader/FullBanner.php');
	@include ('template-parts/ProductMidSlide.php');
	@include ('template-parts/ProductFeatures.php');
	@include ('template-parts/ProductFeaturesSlider.php');
	@include ('template-parts/ProductDimensions.php');
	@include ('template-parts/ProductComparison.php');
	@include ('template-parts/TestimonialSection.php');
	@include ('template-parts/LeftContent-RightForm.php');
	@include ('template-parts/ThreeBoxImage.php');

?>


<?php @include('template-parts/footer.php') ?>
